package com.ecommerce.message.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsEcommerceMessageConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsEcommerceMessageConsumerApplication.class, args);
	}

}
